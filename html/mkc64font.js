/* fontmaker */

const version = 0.1
console.log('mkc64font v'+version)

// TODO: implement saving of characters to fonts
// TODO: make loaded fonts an Array
// TODO: let manipulated fonts be saved/dumped
// TODO: add onChange handler to all the bytes' input fields

// push button functionality
var push = function(){
		var [y, x] =  this.id.split('|')

		// check for toggle first
		var row = document.getElementById('bin'+y)
		var oldbyte = row.value
		var offset = 7 - x
		var value = oldbyte.substr(offset,1)

		// check the toggle state
		if(value == 0){
				// field not yet set -> add
				var newbyte = updateByte(oldbyte, x, 0)				
				this.style.backgroundColor='#333'
		} else {
				// bit already set -> subtract
				var newbyte = updateByte(oldbyte, x, 1)
				this.style.backgroundColor='#fff'
		}

		// update bin
		row.value = newbyte

		// update dec
		var decimal = document.getElementById('dec'+y)
		decimal.value = parseInt(newbyte, 2)

		// update hex
		var hexadec = document.getElementById('hex'+y)
		hexadec.value = ('00' + parseInt(newbyte, 2).toString(16).toUpperCase()).slice(-2)

		var comment = document.getElementById('comment').value
		var comment_bas = ''
		var comment_asm = ''
		if(comment != ''){
				comment_bas = ' :rem ' + comment
				comment_asm = ' ; ' + comment
		}
		// update assembler code
		document.getElementById('code-asm').value = '!byte ' + getHex().join(',') + comment_asm
		// update BASIC code
		document.getElementById('code-bas').value = 'data ' + getDec().join(',') + comment_bas

		// update canvas
		var history = 10
		var bytes = allBytes() //.join('')
//		console.log(bytes)
		var c = paintCanvas(bytes)
		var canv = document.getElementById('preview')
		if(canv.hasChildNodes() && canv.childNodes.length >= history){
				canv.removeChild(canv.childNodes[0])
		}
		canv.appendChild(c);
}


// update the row
var updateByte = function(b, x, m){
		if(m==0){ // adding
				var dec = parseInt(b, 2) + Math.pow(2,x)
		}else if(m==1){ // subtracting
				var dec = parseInt(b, 2) - Math.pow(2,x)
		}
		var bin = parseInt(dec, 10).toString(2)
		// left pad result
		return ('00000000' + bin).slice(-8)
}


// get all bytes in one string
var allBytes = function (){
		var bin = []
		var element = document.querySelectorAll('input.bin')
		for(i=0;i<element.length;i++){
				bin[i] = element[i].value
		}
		return bin
}

// get decimal values of each row
var getDec = function (){
		var dec = []
		var element = document.querySelectorAll("input.dec")
		for(i=0;i<element.length;i++){
				dec[i] = element[i].value
		}
		return dec
}


// get hexadecimal values of each row
var getHex = function (){
		var hex = []
		var element = document.querySelectorAll("input.hex")
		for(i=0;i<element.length;i++){
				hex[i] = element[i].value
		}
		return hex
}


// write canvas
// var bin = Array()
var paintCanvas = function (bin){
		var c = document.createElement("canvas")
		c.width = "32"  // width of the canvas
		c.height = "32" // height of the canvas

		var ctx = c.getContext("2d")
		ctx.scale(4,4)
		ctx.fillStyle = "rgb(0,0,0)"

		// write pixel in the 8x8 char matrix
		for(y=0;y<bin.length;y++){
				for(x=0;x<bin[y].length;x++){
						var v = bin[y].substr(x,1)
						if(v == 1){
								ctx.fillRect(x,y,1,1)
						}
				}
		}
		return c
}


// make charset loop
// var string = String (of the whole charset)
var charset = function (string){
		var id=0
		for(f=0;f<(string.length/16);f++){
				var hex = string.substr(f*16, 16)
				var bytes = hex2bin(hex)
				
				var c = document.getElementById('charset')
				var a = document.createElement('a')
				a.setAttribute('href','#')
				a.setAttribute('onclick','loadChar("'+id+'|'+hex+'")')
				a.appendChild(paintCanvas(bytes))
				c.appendChild(a)
				id++
		}
		
}


// change font
var changeRom = function(r){
		const parent = document.getElementById("charset");
		while (parent.firstChild) {
				parent.firstChild.remove();
		}
//		console.log(rom)
		charset(rom[r])
		loadChar('0|'+rom[r].substr(0, 16))
}


// hex to bin converter
// var hex = string of hex value pairs
var hex2bin = function(hex){
		var bytes = []
		for(b=0;b<hex.length/2;b++){
				bytes[b] = ('00000000' + parseInt(hex.substr(b*2,2),16).toString(2)).slice(-8)
		}
		return bytes
}


// load character
// var hex = string of hexadecimal representation of char
var loadChar = function(str){
		var [id, hex] = str.split('|')
		var bytes = hex2bin(hex)

		document.getElementById('charid').value = id

		// clear canvas first
		resetChar()
		// clear comment
		document.getElementById('comment').value = ''

		// put values in place
		for(y=0;y<bytes.length;y++){
				document.getElementById('bin'+y).value = bytes[y]
				document.getElementById('dec'+y).value = parseInt(bytes[y],2)
				document.getElementById('hex'+y).value = ('00' + parseInt(bytes[y], 2).toString(16).toUpperCase()).slice(-2)
				
				for(x=0;x<8;x++){
						var offset = 7 - x
						if(bytes[y].substr(x,1) == 1){
//						console.log(y+'|'+offset)
								document.getElementById(y+'|'+offset).style.backgroundColor = '#333'
						}
				}
		}

		// update assembler code
		document.getElementById('code-asm').value = '!byte ' + getHex().join(',')
		// update BASIC code
		document.getElementById('code-bas').value = 'data ' + getDec().join(',')
		
}

// reset the canvas
var resetChar = function(){
		for(y=0;y<8;y++){
				// set binary, decimal and hexadecimal to zero
				document.getElementById('bin'+y).value = '00000000'
				document.getElementById('dec'+y).value = '0'
				document.getElementById('hex'+y).value = '00'

				// put all cells to white
				for(x=0;x<8;x++){
						document.getElementById(y+'|'+x).style.backgroundColor = '#fff'
				}
		}

		// clear preview
		var preview = document.getElementById('preview')
		while (preview.firstChild) {
				preview.firstChild.remove();
		}
		
}

// initialize the fields
var init = function (){
		// print chars
		charset(rom['us'])

		const cells = document.querySelectorAll("td.col")
		for(i=0;i<cells.length;i++){
				cells[i].addEventListener("click",push,true)
		}

		// load first char
		var str = "0|"+rom['us'].substr(0,16)
		loadChar(str)
		
}

const dummy = '3c666e6e60623c00183c667e666666007c66667c66667c003c66606060663c00786c6666666c78007e60607860607e007e606078606060003c66606e66663c006666667e666666003c18181818183c001e0c0c0c0c6c3800666c7870786c66006060606060607e0063777f6b6363630066767e7e6e6666003c66666666663c007c66667c606060003c666666663c0e007c66667c786c66003c66603c06663c00'

